---
title: My projects
image: /images/idea-1876659_640.jpg
permalink: /projects
layout: page
---

## Yamichain
**Role:** Developer

**Duration:** 02/2021 - 03/2021

Yamichain is a decentralized application that makes it easy for micro-finance provides to easily and securily share 
information that allows them to easily identify trustworthy customers to lend money to.

**Links**: -

## OLPy
**Role:** Developer

**Duration:** 11/2020 - now

OLPy is a Python package that implements a series of 16 online learning models using an API similar to that of scikit-learn.
Its main purpose is to make it easy to anyone willing to train such models to do so.

**Links:** [Github][olpy-github] - [PyPi][olpy-pypi] - [Read the Docs][olpy-readthedocs]


## Lynxmotion AL5D robot simulator
**Role:** Developer, Research Intern

**Duration:** 06/2020 - 08/2020, 02/2021 - 05/2021

This is a simulator for the Lynxmotion AL5D robotic arm. It has been built with the Unified Robot Descriptor Format (URDF) with 
a set of tools to control it.

**Links:** [Github][al5d-github]

[olpy-github]: https://github.com/boladjivinny/olpy
[olpy-pypi]: https://pypi.org/project/olpy/
[olpy-readthedocs]: https://olpy.readthedocs.io/en/latest/
[al5d-github]: https://github.com/cognitive-robotics-course/lynxmotion_al5d_description
