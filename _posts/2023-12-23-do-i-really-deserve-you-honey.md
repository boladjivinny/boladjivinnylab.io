---
title: "Do I really deserve you, honey?"
date: "2023-12-23T13:11:00-05:00"
layout: post
image: /images/2023/couple-560783_1280.jpg
---

As I woke up today  
Lights still off, my blanket on  
Your face was stuck right on my eyelids  
I couldn't see it, but my smile seemed vivid.  
I could feel it from the expense of my jaws  
They went wide, and a peaceful sensation traversed me  
From head to toe, leaving my heart warm, honey...  

And then I opened my eyes  
But you weren't there  
At that moment, I felt like I miss you  
Of course I do, everyday, but not like today  
I miss your smile, I miss your laughter  
I miss the sound of your voice, how it makes me shiver  
I miss hearing your breath, but also the lack thereof  
I miss your touch, I wish they last forever  
And I miss your eyes, just how easily they can make me fluff  

Kind and welcoming they always seem  
As weird as it sounds, they tell me so much about your  
Love, I just can't help but be amazed at how you look at me...  
I know, and trust when you say "I love you", it just feels stronger when your eyes say it.  

As I scrolled through my phone  
Delighted with the amazing memories we had together  
Jovial moments, no matter what, or where  
In the sand of the beach where I wrote your name,  
Besides that tree where we had that picnic  
I still remember our first kiss, how could I forget.  

I wish I could get more of that  
I want to take you places  
Where the scars are rare, and at the stars I stare.  
Close to the stars you always seem to take me  
I wonder if I really would look at them, when you are there  
Delighted I always seem to be when you are around  
Talking of which, I miss you asking me why I keep staring at you  
Always embarrassed at the question, today I remove the mask  
I stare at you in amazement at how pretty you are  
You look so beautiful, I sometimes wonder if you are real.  

And if all of this was a dream, I wish to never wake up  
Life with you is so beautiful.  
Effortlessly you make me feel whole  
And I wish to have you forever, by my side.   

You promised you will, and I trust your word  
I just cannot help asking myself under my breath  
Each time you make me feel like the center of your life  
"Do I really deserve you?"  

Today I had the courage to answer.  
And I want you to hear it baby.  

I haven't the least idea whether I deserve you  
But if a God assigns a man to his woman  
I hope he assigned me to you, in this life  
For I am blessed to have you  
I hope you feel the same  
And if there must be another life after this  
I pray that I get to be assigned to you again,  
And again, and again, till times end,  
But still, our love, I hope, will stand.   


> Your man...

Image by <a href="https://pixabay.com/users/bingodesigns-213864/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=560783">Bingo Naranjo</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=560783">Pixabay</a>