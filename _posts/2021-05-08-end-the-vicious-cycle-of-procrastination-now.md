---
title: 5 lessons learned in my journey away from procrastination
date: 2021-05-08T11:27:12+02:00
layout: post
image: /images/2021/pedro-da-silva-unEmGQqdO7Q-unsplash.jpg
tags:
  - Book Review
  - Personal Experiences
  - Motivation
---

**December 2020.**

We are towards the end of the semester. Final exams, projects, and everything alike had to be finished and submitted. And while I was really working towards meeting all my deadlines, I really was feeling that I was underperforming myself. And this was affecting me in every single aspect of my life: personal, relationships, professional, etc. I was literally just trying to keep my water out of the water and this irritated me because a few months earlier, I was literally on top of my game. The story goes as below:

**August 2020.**

On the verge of starting the Fall semester with all the seriousness that is required from a second-year student taking a research project (representing the quarter of my final grades), I was just presented with the opportunity of engaging in martial arts training. I always wanted to have a better sense of discipline in my life (somewhere still feels like joining the armed forces one day 😎) so when the opportunity came, I just did not give it a thought. I was in right away. Ready to welcome Vinny Sensei 😌.

**September 2020.**

I have been going for a couple of weeks to my martial arts training now. Spending more than 6 hours every week doing just that and still finding time to partake in other fun activities on the side. But remember, I have school work to do. An academic suicide you will say. Well, it actually turned out to be the complete opposite. I broke my own records of performance over the two months that the training lasted. Submitting my assignments way before the deadlines, actively assisting a professor with his brand new class, managing two clubs, and working on my research project as well. I was literally floating by then. But, came October, things collapsed.

**October 2020.**

This is the middle of the semester at my university. My appointment as a teaching assistant just expired, I was jobless. Coincidentally, my Karate training was over as well (just got a yellow belt).

![I during one of my martial arts training](/images/2021/karateka.jpg)
_Me during my final Karate exam last year._

From that very moment, I was really taking too much time to work on things. Everything just became a burden so heavy to carry that I started feeling very hopeless. Thank God and his sons and daughters, you can always find answers in other's experiences. I like reading so this was my time to learn from someone else.

**November 2020.**

My struggles have been going on for one month now. The saddest part was those days where I would go for a nap and my first reaction when I wake up was to look at the time and literally shout "Oooh shit! Fuck!" because I was feeling like I just spent a big amount of my day sleeping and not working. A workaholic you would call me, probably I answer.
Nonetheless, this was an issue because I would still spend time after those naps to get back to work. I certainly was not doing any good to myself. By the biggest of fortunes, while searching on Kindle, I came across a book called "The gifts of imperfections" by Brene Brown. And it taught me how to accept that I am imperfect and not push myself too much. That was all I needed to get back to my (almost) better self. I managed to survive the semester in that new mood and it was good. Then again:

**December 2020.**

The Fall semester just finished and I am back in my lovely home country for the holidays. This is supposed to be fun but then how do you manage to have fun when there is that paper you are supposed to write about but you are just not doing it. I really felt bad because I knew this was something I had to do but I just could not figure out when and where to start. I was for sure procrastinating. It eventually turned out that I was full of fear of producing some imperfect work that I just could not figure because perfection is hardly attainable. This is one of the biggest lessons that I learned from reading the book "The Now Habit" by Neil Fiore. And over the months ever since, I have had a certain number of experiences that I am sharing here as 5 important points that I believe could take out of the vicious procrastination cycle.

## 1. The power of words
One of the alumni of my university recently said something with
which I totally agree. He was saying:

> You cannot force yourself into doing something even if you
know it is for your best. You have to negotiate with yourself.
>
> <cite>Kizito</cite>

I agree with him because in many instances I would want to push myself in waking up, or cooking, or even working on an assignment but the best that would happen is that I would blame myself for not being doing well. Which eventually will make me feel even worse when the work does not progress as I wish. And this is because deep in my mind I will be thinking "I have to do this" which removes the power of choice from me. I would unconsciously be forcing myself to do something but I for sure do not like to be ordered. I learned this from "The Now Habit" where the author recommended using the verb "to want" to express the things that need to be done. I know it is difficult to express something that someone else asked you to do (an assignment for example) but doing so makes you own the task and believe me, you do much better in these conditions.

![Say you want instead of you have to](/images/2021/want-not-have-to.jpg)

## 2. Do your perfectly human work
For most of us, at least for me, we stumble upon some tasks for which we feel like we must give the best we can, this work should be perfect. I for sure have suffered from this for a while now. I remember being talking with someone last year and telling them how I get anxious when on the edge of finishing a programming project. What I could not understand by then was that I was actually scared of delivering the project and be confronted with some of its imperfections. I just could not stand anything imperfect being related to me out there 😞.

But while it is a good endeavor to wish for perfection, in many
regards it prevents us from moving from a point to another 
one. I learned this over the past two semesters where I had to
write a lot. After talking about my lack of inspiration to
my supervisor, he motivated me to write at least every single
day even though I feel not inspired. And to make his point, he 
used the following quote.

> You have to write when you’re not inspired. And you have to write the scenes that don’t inspire you. And the weird thing is that six months later, a year later, you’ll look back at them and you can’t remember which scenes you wrote when you were inspired and which scenes you just wrote because they had to be written next.
The process of writing can be magical. …Mostly it’s a process of putting one word after another.
>
> <cite>Neil Gaiman</cite>

Despite not being able to write every single day (sorry Prof.), I still manage to write some days I just did not feel inspired because well, a researcher has to keep writing. The same applies to any other field of work that one has to do. So whatever you are doing right now, however big and daunting the task might be, just get started on it with your humanly perfect work. You will be amazed by how the small steps will pay off in the long run.

![The power of small steps](/images/2021/small_steps.jpg)
_-- The power of small steps_ 

## 3. Every goalscorer deserves to go to the corner
**June 2020.** 

The Spring semester has just ended and my summer internship had started a few weeks earlier. In particular circumstances, the COVID-19 pandemic was hitting and for once I was working (not studying) directly from home. And that meant sharing the living room with my other roommates for working. So at the end of the day, I was still around people, no major difference than if I was in an office.

At least so was I thinking. But then not once or twice, but many times over the period of the internship, I had my "small wins" that I always feel the need to celebrate loudly and for which I always feel very happy. But that won me the right to be dubbed as "the man who goes to the corner" by my housemates. This refers to the celebration of sports professionals (soccer players especially) when they score a goal. And while I am sure they did not mean to make me feel bad, I eventually started feeling bad questioning whether those celebrations were worth it, if I was not overdoing this. That was one of my worst existential crises ever because I started taking for granted all the things I have done, questioning their worth and my own skills as a matter of fact. And to make matters worse, there was this quote from President Patrice Talon of the Republic of Benin that kept coming to mind. This is just an attempt at reproducing what he said.


> Along the way, we will make a number of small victories. But I
> like keeping my mind on the bigger picture and on the greatest
> achievements that we can do rather than contemplating the 
> small wins done.
>
> <cite>Patrice Talon<cite>

He is right in one sense, one should not remain in awe in face of the small wins. But that does not mean they do not deserve to be celebrated, at least for me they are. So by then I literally stopped enjoying my achievements unless they are really big, downplaying my own work at the time. Fortunately, I got back on the good thoughts after reading "The Now Habit" where the author specifically suggested to take time to reward oneself for every work successfully and human-perfectly accomplished. They explained that it allows keeping motivated and do even more great work. I can testify for that so I guess you should give it a try.

![Meme](/images/2021/had_us_in_first_half.jpg)

## 4. _"Everyone to whom much was given, of him, much will be required, and from him to whom they entrusted much, they will demand the more"_ Luke 12, 48
Some will tell you that there is no better book of wisdom than the Bible, and with that, I totally agree. I personally do refer to many to direct me at times and so did it happen in my struggles with procrastination. So you might be asking yourself, how does this verse fit in a procrastination discussion. Here is the explanation.

Over the two semesters, what I noticed was that around the middle of the semester where I happen to have fewer duties, I tend to perform really worse than when I am very busy. And my own explanation to that falls in the two following points:


1. When you have more to do, you try to strategize in order
to get the most done. But when you have very few objectives,
you feel that you have too much time at hand and end up
procrastinating.
2. Having many different things to work on allows you to move
your focus from one activity to another. Believe me, when you
are stuck on a really challenging problem, being able to think
about something else really helps.

Going back to the verse now, the point that I am trying to make is that when you are being assigned to amazing (and challenging) tasks, it is because you are believed to be able to complete them. And the expectation is that you have to meet the expectations. But do not get me wrong, I am not saying that you should do perfect work but that you should believe in yourself for giving the best out of yourself.

So my advice for this is to find ways to allow yourself to
have alternative ideas when things get rough. You will thank me
later.

## 5. Asking for help does not make you feel less smart
> If you want to go fast, walk alone. But if you want to go far,
> walk together.
>
> <cite>Unknown author</cite>

This quote perfectly encompasses what I want to say here. It is
totally fine to get stuck on things but the bottom line is that
you should not remain stuck. This had happened to me many times
where I could not solve some problems that eventually stand
on my way to finishing off something. My old self would usually
remain still and try to find the solution on his own (that
does work sometimes) but there is usually always someone 
around you that can point you in the right direction. Or at 
least talk to someone and you will see how you had the answer all that time.

## Final words
Procrastination is not something that you solve once and for all. As for many things in our lives, it is an all-time fight you need to keep working on. I have started, stumbled a few times again but for the sake of getting my better self, I keep working on it. I certainly do not think that I know how to avoid it but I really hope my advice and story do resonate with you and that those lines (sorry for the length though 😅) would help you find your way out of procrastination.

I am really looking forward to your personal experiences in the
comments. 