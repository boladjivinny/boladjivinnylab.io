---
title: "The Road Ahead: A Mini-Memoir"
date: "2024-12-15T12:00:00-05:00"
layout: post
image: /images/2024/walking-pedestrian-chatgpt.webp
---
To know or not to know, I always wonder which one is better. 
Knowing had been my safe bet for a while until, on a fateful morning in September, I made a decision that let me find out something I somehow wished to remain oblivious to.

It all started like any other Thursday morning. 
As I exited my apartment, I reached for my keys in my back pocket and locked the door before hastily taking the stairs down to the community's gate. 
There, I found the "Exit" button, which seemed to woo residents into getting out of the building with its dark green color and the bright source of light that illuminated it. 
I needed no convincing, so I pressed the button in a matter of seconds and reached for the door's handle almost instantly. 
With a sharp push backward, I made a way through the door for myself and, in so doing, clocked in the first of my two arm workouts for the day.

As I reached the ground, I looked around for a rentable electric scooter. 
There were none in sight, so I decided to walk to the bus stop. 
By the time I reached the first all-way stop sign on my path, it was 06:48 am. 
I took the only way that led to my intended destination. 
Three minutes later, I was at the second all-way stop sign on the trajectory. 
Unlike the first one, where I could only go right, I had the choice between two routes: the downhill path to my right with its commercial vibes and the homely road ahead of me. 
In my time-sensitive situation, taking a downhill path seemed incongruous, so I kept walking straight. 
I barely passed the second house on the way when I heard a familiar sound. 
It reminded me of the decrescendo noise of a Yamaha bike's motor between its apparent start and its sudden, unexpected halt.
In the car-ridden city of Atlanta, only one thing sounded like that: the MARTA bus braking.

And sure enough, there it was, the MARTA bus I intended to take. 
It had stopped at a red light two intersections after my bus stop. 
At 06:54 am that morning, I knew I had missed my bus. 
Then I wondered if it would have been better if I had taken the downhill path and did not see the bus leave.

I felt guilty for not abiding by my friend Frederic's principle of arriving at the bus stop early. 
Every attempt at excusing my behavior led to arguments that failed to convince my most gullible self. 
My self-deprecating humor could not calm me down either, so I reached for my phone, which I believed would distract me during my wait. 
While double-checking my itinerary, I soon realized that the bus I saw was twenty minutes late and the one I came for was set to arrive in ten minutes. 
Almost immediately, a blushing smile appeared on my face, and in Frederic's court of lateness, I had a case.

I was everything Instagram's soft porn could not get me to be: calm and confident. 
I even indulged in believing that the next bus would arrive on time, but that did not happen. 
By 07:16 am, I found myself an avid spectator of a show of passing cars. 
One after another, I saw in no particular order the Chryslers and the Chevrolets, the Hondas and Toyotas, and the occasional truck heads with three front passengers.
But of all the vehicles I've seen that day, the Cyber Truck with the pink carrosserie stood the test of time in my memory. 
I was looking at the car so intently that I almost missed the opportunity to reciprocate the "I see you, brother" nod a Black pedestrian was giving me at that time. 
My senses came back right on time, and I gave the nod alright. 
But as I shook my head up and down, I wished it was something else I was waving in the air. 
Like my hands, for instance, to request a ride from a passing car. 
But in my two years in the city, I had never seen anyone do such a thing before. 
And even if I did, my negligible success rate at the ordeal back in Benin was a strong enough deterrent to even trying.

To be fair, what I tried multiple times would be more appropriately referred to as "moto-stop," for I only wished for motorcycles to stop. 
There weren't that many "montos" --- cars in many Beninese dialects --- on the streets anyway. 
But of all the bikes on the road, only a few dared to pay attention to my waving hands, and of those who did, I could count on my hands how many stopped to hear my plead. 
For every decade I have lived so far, only one of those strangers agreed to help me out. I was clearly no good at this and wasn't going to try.

However, I would have loved the possibility of stopping a motorcycle taxi. 
At least with the taxis, I was guaranteed a hearing and, eventually, after a substantial bargain, an invitation to sit at the back to get to my destination. 
It amazes me how different yet very similar the Okadas of Lagos were to the Mottos of Kigali, who reminded me of the Zemidjans in my childhood town of Gogodohondji. 
Besides the fact that the Mottos wore sleeveless red vests rather than the customary yellow shirts of the Zemidjans, it was calling the bike man "boss" instead of "kekenon" that took me the longest time to adjust to. 
The skill of negotiating my trip's fare, on the other side, transferred quite smoothly from one place to the next. 
My experience with Zemidjans had taught me the dos and don'ts of the craft. 
It also taught me many life lessons, one of which seemed very applicable to my current situation.

It happened on a Sunday morning when the sun came out pretty early. 
It was during the extended holiday breaks between June and October when the Sunday mass was the only chance to dress nice. 
On that day, I adorned a bomba, a traditional tunic and trouser ensemble made with Ankara fabric. 
With every sensation that the clothes perfectly adhered to my physique, my ego stretched a little. 
But it shrank even faster once I noticed dust accumulating on my pair of black espadrilles the more I walked on the road to the St Michel de Togoudo parish, constantly turning my head backward to spot a Zemidjan without a customer. 
Unfortunately, it was one of those days when all the Zemidjans were either booked or headed in the opposite direction. 
At last, almost midway to the church, I found one who asked 150 FCFA to drive me for the remaining one and a half miles of the journey. 
I found his asking price ridiculous, given that I would have paid the same amount if the trip started closer to my house. 
His complete disregard for my starting point made me realize this: in life, the only things that matter are where you are and where you are going.

On that day, I ended up on the back of the Zemidjan's bike for 100 FCFA, heading to the church, where I was once taught not to look back after putting my hands on the plow. 
This time, I was at a bus stop in a city without Zemidjans and was heading to a Toastmasters meeting.