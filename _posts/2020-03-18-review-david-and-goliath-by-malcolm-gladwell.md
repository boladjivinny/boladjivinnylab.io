---
title: What if difficulty is what you need(ed) to get better ?
date: 2020-03-18T13:06:07+00:00
layout: post
image: /images/2020/03/overcoming-2127669_1920.png
tags:
  - Book Reviews
---
Some few days before the end of my last semester, we were having an end of semester dinner with all the scholars with my school. Imagine a round table where are sitting a lot of graduate students who just finished their exams. They are all eating, drinking, joking and have a well deserved good time.  That is a perfect setting for relieving whole semester stress and just feeling good. And yet that night, my heart was having a just out of happiness. I was so worried about spending my holidays away from home, the first time since in my life. I remember describing this upcoming period as "the most boring holiday" I've will ever spend.

![](https://media.giphy.com/media/TU76e2JHkPchG/giphy.gif)
*Sad me thinking about my holidays*

## Quid of those holidays?

Fortunately enough, it turns out to be a very good holiday season. Not because I partied a lot (I didn't) but mostly because I worked on one of my biggest passions, reading. I read a total of 6 books in 1 month. And reflecting on this, I realized how those words from Malcolm Gladwell applied to what I lived. I just realized that my difficult situation brought me more good things than I was expecting.

As I was saying previously, I read a total of 6 books during the festive season. Here is a list of those books in the order I read them:

  1. David and Goliath, Underdogs and the Art of Battling Giants, _Malcolm Gladwell_
  2. The Alchemist, _Paulo Coelho_
  3. Reallionaire: Nine Steps to Becoming Rich from the Inside Out, _Farrah Gray_
  4. Start-up Nation, _Dan Senor, and Saul Singer_
  5. 21 Lessons for the 21st Century, _Yuval Noah Harari_
  6. Networlding: Building Relationships and Opportunities for Success, _Jocelyn E. Carter-Miller and Melissa Giovagnoli_

![](https://media.giphy.com/media/NFA61GS9qKZ68/giphy.gif)

## Finding "you" in the "U"?

This post will specifically be talking about the first book on the list, "David and Goliath". In this book, Gladwell gave practical advice on how to succeed when all the odds are against you. I was amazed by how Gladwell told the story of the prowess of the small David battling the giant Goliath. The basic idea of his explanation is that David just didn't do what others were expecting him to do. Throughout the book then, Gladwell explained many theories of unexpected success and reactions. He mostly talks about the "reversed U" effect (for my geek friends, this is like a Gaussian distribution) which states that at some point, some working strategy can become ineffective or unproductive if used above a certain limit. And to explain this, he referred to situations in Basketball, justice and even warfare.

![Gaussian distribution or reversed U](/images/2020/03/normal_distribution.png)

## Why you should embrace difficulty?

But the part that touches me and in which I found myself in was when he started putting difficult situations as a way to make great achievements. He started talking about dyslexia and making a very weird connection between people having this disease and great success. And then he went on to use as evidence the fact that a lot of leaders have lost a parent in their childhood and that this seems to have fostered their character.

This eventually comes to what we hear all the time about taking a risk but I believe this is just not about taking a risk. Imagine taking a risk because you're just expecting an outcome out of it, let's say a million dollars. For sure this might work but what I'm talking about here is to challenge yourself a bit more all the day toward achieving your mission in the world. Believe me or not, we all have a great contribution to bring to our world and making that happen is the only way to find true happiness.

Put this scenario into perspective: you're alone, walking on a road full of dangers. Far away you can see the light, very tiny but sufficiently strong to allow you to see. You feel like going to that light cause it's so beautiful and seems so peaceful. Yet on the road, you find some nice places where you can stand and end your journey, would you like to. Those places represent your comfort zone and trust me, even though they look nice, you certainly deserve more.  You are meant for bigger purposes and it requires you to be dedicated and intentional to take on challenges that will take you there.

<img class="aligncenter" src="https://media.giphy.com/media/d3mlE7uhX8KFgEmY/giphy.gif" /> 

## My difficulties

I remember at my last job choosing to do what was not being asked for, adding an extra burden on top of what I already have and I think it eventually pays off. One of the biggest achievements in my developer career was to learn, almost all on my own, how to build and customize extensions for two of the most use CMS in the world (WordPress and Joomla).

My memories also took me back to the surprise of my friends and colleagues when they first come to my place and see a room full of English books, complaining about first not liking reading and next, not understanding a lot of English to read English books. Yeah, it made me feel like I was doing too much of this but today I look back and [feel proud of those choices I made][multi-language]. Not late than this year was I discussing with a friend in Rwanda who told me that I speak like Nigerians. But God knows that I spent the biggest part of my life in Benin, speaking French and that only one night saw me sleeping in Nigeria. Despite that, because I took on that extra load on my own, it's easy for me to survive this change of learning system.

## **Final words**

I believe that taking on extra load is highly beneficial to you if this load is worth it. It participates in building what many experts in leadership refer to as **grit**. So, is it a course that scares you that you want to take? Or take the risk of proposing to someone you like? Or even making a total career change? Whatever that thing is that will make you the best version of yourself, don't give yourself a choice. Do it right now and enjoy it later.

This appears to me as a shameful statement from me, the guy that is suggesting you praise difficulty in your life but I will say it anyway. I started drafting this article since January 😓 and have been delaying its publication ever since. I was feeling bad about myself until last week when I read "Outliers" from Malcolm Gladwell. This got me more confident about posting this article.

In a nutshell, Gladwell who has a very impressive writing style was explaining why people succeed without coming up with a silver spoon in their mouth. One thing I learned from the book is that being a genius or having talents doesn't guarantee success. What's more important is an intense practice that eventually differentiates success stories from others. In Gladwell's words, you need to practice for a total of 100,000 hours to master a skill or subject. So I reiterate, _would you like to be among the best in your field, start pushing yourself today and see how it pays off later._


**_Cheers..._**

[multi-language]: {% post_url 2019-12-31-my-journey-to-multilingualism-tips-and-tricks %}
