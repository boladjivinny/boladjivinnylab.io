---
title: Get that advice that makes sense to you
date: 2019-12-21T00:18:53+00:00
layout: post
image: /images/2019/12/advising.jpg
tags:
  - Brainstorming
  - Lifestyle
---

"Life is about choices", a saying in my home country goes. This is particularly true when we are to make decisions that affect our lives and those of people around us. When faced with this kind of decision, we sometimes feel like making the wrong choice, doubting our own beliefs. Hopefully, as people that bond with others, we get the chance to talk to people with the aim of seeking any kind of advice that can help us make the **best** decision. Those people we rely upon can be friends, siblings, parents, mentors, instructors, or any other people we trust. But in seeking advice, it is important to do it wisely so that we don't end up feeling people for giving us advice we voluntarily ask for. The same goes for people giving advice who can well end up not helping at all. Having witnessed and experienced such dilemmas many times, I would like to share some tips I've learned throughout the way for people seeking advice as well as those who are giving them.

![helping other, illustrating adivce](/images/2019/12/zXqsee_Wwc2MUVETYNDO8Ur2zisfG7righgiKlebNBotSp46C9PjrhxxBGzyX1Q_g9E4fh0Cv4l_CegB5cykSO3tzTGxIi7N4RaWjGznZeu5ewG2oo4C6bwHT77xyMnyCjczxXX0Z161ovg4kkDalFp2b2I8qcbStoTy6i3bMw4Uli.jpg)

## When thou face doubt, aim to ask for help

A few weeks ago, I went into a dilemma of a course choice because two classes I want to take are clashing and I have to take only one of them. Making this decision is really difficult for me because both classes are equally interesting and important for me: one is about Ethical Hacking and the second one is about Data Structures and Algorithms (DSA). Just to give you a picture of how hard it is for me to decide, I've been signing and dropping for those classes over and over again since registration started. And in a school like [Carnegie Mellon University Africa](https://www.africa.engineering.cmu.edu/), there are so many good courses but at some point, you have to make your mind.

Building on the support available, I went on to get the various points of view. First-person I went to was my faculty adviser who happens to teach the DSA class. After listening to my concern, he went on to ask me some questions about my background before advising me to take the DSA class to get a better foundation in the subject. I then went to present the same problem to one of my instructor who advised to take the Ethical Hacking class instead. Same advice from my second-year mentor for whom tough classes are the best to take. Well now I had three different informed inputs and yet I cannot still find an answer to my question: **Which of those classes am I going to take?** I mean how do you choose between two things you can hardly separate? For me, it is a really tough problem and the advice I collected didn't really help me out. But it was not because they were bad, but I rather took them the wrong way.

> The advice that I've gotten was not bad, I was just not in the right mindset.
>
> <cite>Vinny.</cite>



## But thou should ask for advice, not for a solution

Doing some auto criticism of my approach, I came to realize that I went to those people expecting them to make the choice for me. What then happened is that I was not convinced by what I've been told? The reason for that is instead of finding arguments for either of the options, I was just looking out for solutions. What would then happen if I ended up following someone's advice and things don't go well? Should I hold them responsible? Sure I shouldn't but deciding this way, I'm most likely to do so.

Looking back at some key moments of my life, I seem to have been doing this for a while. It may not be common practice but I think we all do it at some point. This kind of behavior eventually prevents us from living the life we want. Love stories, careers, and relationships can all suffer this behavior. In the light of this reality, I went on to realize some critical thing. One should not seek solutions, we should rather find arguments to support our final decision. Here is my quick draft approach on this for getting advice :

  1. Have your list of choices and possibly some arguments for each of them.
  2. Discuss them with people who can give you informed advice. But remember that quality is always better than quantity.
  3. While getting advice, listen carefully to their arguments. By doing so, you may end up finding a solution that you didn't even think about in the first place.
  4. Commit all the advice that people gave you and make your own judgment.
  5. **Most importantly, own your decisions and the consequences that come with them.**

As I was talking recently in one of my speeches at [Techy Talkers Toastmasters club](https://twitter.com/techytalkers/status/1182706464625954817), the best you mind tour decision, the best you feel about yourself, and the best you deliver, anytime and anywhere. I think that by going with this approach, better solutions will be made.

> The best you mind your decision, the best you feel about yourself and the best your deliver, anytime and anywhere.



## Quick note for people consulted for advice

I know it is tempting to go into sermons or life lessons when people come to us. But who cares what you're achieved? At some point, you need to realize that life experiences are different. So instead of putting the advice seeker in your shoes, take your time to listen carefully before giving any sort of input. In [Andrew Carnegie's words](https://www.amazon.com/dp/B0811Z2NNF/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1), you should do some emphatic listening, getting to understand what are the motives and needs of the person talking to you. Then you should just tell them what you think would be the right path to choose and why. But never ever try to tell them to take this path or the other. You may find yourself being hated for them making the wrong choice based on your advice.

![father giving advice to kid](/images/2019/12/silhouette-1082129_1920.jpg)

On that note, I would like to thank all who ever provide me any guidance or advice. This article reflects my current view on the subject. Would you have any idea or comment about this, feel free to express your thought. Cheers&#8230;
