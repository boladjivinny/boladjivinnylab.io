---
title: "It is not that cold: A reflection on resilience"
date: 2022-12-31T18:35:01+00:00
layout: post
image: /images/2022/common-dandelion-7207918_960_720.jpg
---
A few weeks ago, I stumbled upon a text amid my end-of-day visualization exploration of my friends’ statuses. It was a brief content, but it hit home. And so very so. My friend was venting about the merits of cold showers in dealing with chronically dry skin. As someone who suffers from that condition, I could not help but contemplate, from a distant location, the old good rainy days in my homeland, which I remember positively affected my sense of sexiness. Out of the blue, this information sparked many questions about the claim's veracity. Therefore, I decided to set myself as a guinea pig for the experiment. Consequently, cold showers have been an unalterable element of my daily routine for the past few weeks. Did it work? Absolutely. To what extent? Superbly I would say, given how negligible the number of skin irritations I had during that time was. But more than the physical effect on its own, the best part of this story is what I learned from it as a human being. Below, I delineate three points that I consider the most important. Stay with me till the end.

## It is not that cold.
Until recently, my first reflex when I was expecting the water in the shower to be cold was to use my thighs as a sensor to know when I was ready to bathe with it. I wonder how I learned that, but that is a conversation for another day. The interesting part of this process is that I was somehow getting my body to react, but the first step was to let my brain realize that the water was cold. Have you ever noticed what happens when you suddenly pick up an injury? For the very self-aware person, you tend to observe a short delay between the triggering event and the feeling of pain. The image below is a good illustration thereof.

<figure class="image">
  <img src="https://cdn.hswstatic.com/gif/cipa-pain-diagram.gif" alt="How pain works">
  <figcaption>How pain works</figcaption>
</figure>

As you might have seen and as it is better explained here, we can only feel the pain after the brain gets the information from the affected nerve. What happens instead when you have set your mind to feel the pain (or any sensation) is that it gets you ready. Eventually, your reaction is a replay of some learned mechanisms that could be inadequate to the actual temperature of the water.  I was not aware of that until I started being more attentive. That is when I realized that my reaction was always the same, no matter how cold the water was. The realization was more intense at the time when, in the beginning, I would shift from tidy to slightly cold water. For some reason, I would feel my whole body instinctively getting ready for the “threat”. Paused breathing. Tight muscles. Shaking. Well, you name it. By getting past those acquired feelings, I learned to appreciate the cold water more, feel it and just vibe with it. It only took an effort to shut down my brain’s defensive mechanisms to let myself feel the water just the way it was. And trust me, it is not that cold.

> Fear is more pain than the pain it fears
>
> <cite>Philip Sidney</cite>

## The resilient edge of resistance.
It is a concept I learned from my readings on Tantric rituals, mainly as they apply to erotic body massages. It refers to the maximum intensity that you can utilize on a surface before it starts pushing back. Go softer, and there is no feeling. Go a bit stronger, and you have probably undone what you tried building. The exciting part is that, as you remain long enough at the edge, it starts to feel more comfortable and thus pushes itself a little farther. Then growth happens. I relate this concept to what we often refer to as the ‘comfort zone.’ Below, I depict each zone as a circle. As seen in the drawing, the resilient edge of resistance resides at the boundary of our comfort zone and the “discomfort zone”. In the case of my story, My journey with cold showers started with mildly cold water mixed with warm water and progressively turned into entirely cold showers. Growth happened between those phases, and I was ready to push a little further.

<figure class="image">
  <img src="/images/2022/comfort_zone.svg" alt="Comfort, discomfort and panic zones">
  <figcaption>Comfort, discomfort and panic zones</figcaption>
</figure>

I picked up a similar concept from attending a TedX Talk this semester where the speaker (in the picture below with me) was talking about intentionally seeking discomfort in our daily lives. And they gave an example of a parent seeking to increase their kid's knowledge in mathematics. Out of three problems with increasing difficulty, each corresponding to a circle in the drawing above, they argued that the one that will help the student learn more is the one in the discomfort zone. That is because the one in the comfort zone would be too easy, lest they get bored. The one in the larger shape is likely too difficult for them and would only help make the student hate mathematics even more, ultimately defeating the purpose of the learning experience. The moral of the story is that in seeking discomfort, you need to ensure that you are putting in the right amount to get the desired benefits.

<figure class="image">
  <img src="/images/2022/tedx_gatech_picture.jpg" alt="Picture from the TedX event">
  <figcaption>Picture from the TedX event</figcaption>
</figure>

## “For whoever has, to him more will be given, and he will have abundance” - Matthew 13:12
A common conception wants the act of dressing nicely to boost someone’s confidence. While I agree to some extent with that statement, it seems like a shallow treatise of a much deeper phenomenon. I refer to it as “pivotal ascension,” which I define as deriving confidence from achieving something worthwhile for another cause. In the case of dressing nicely, I believe that the sense of worth comes not from the visible part of our outfit but more from the pride that stems from assembling the components into a beautiful whole. And then, we build on that achievement for the rest of the day. And with each successful task in the day and every ounce of compliment, we get more boost and confidence. My own experience of cold showers serves as a way to remind me every day that if I set my mind to something, I will do it as long as I can find a way. I want you to keep this in mind while pursuing your best self.

I conclude this blog by wishing you a happy new year. May grace, hope, and growth meet you every day of the year. Allow yourself to jump under the stream and reap the fruits of your boldness. Remember, **it is not that cold**. 
