--- 
layout: post 
title: Adjacent memory buffer overflow attack explained 
date: 2020-05-18 09:29:35 +0200 
image: /images/2020/05/mahmudul-hasan-shaon-txfmOG0lJ20-unsplash-scaled.jpg
tags: 
- Cybersecurity 
- Hacking 
- Geek Zone
--- 

In the world of cybersecurity, buffer overflows attacks have been for years among the most critical vulnerabilities affecting software applications [^1]. 
They can be defined as a failure of a program to check for user input size which can lead to the execution of malicious code. In a general sense, applications developed using programming languages such as C and its derivatives are more
vulnerable to these attacks. 

The main reason for this is the way
various operations are performed essentially on strings but also on basic data
types such as integers, characters, etc. One key issue here is that
there is a la lack of control on the size of input
with respect to the size of the object that is supposed to
receive the input from the user. As a solution to this, multiple
functions have been implemented as a substitute to existing ones, with a
parameter specifying the size of the input that should be received from
the user. The table below shows a list of those functions that
work on strings in the C programming languages. 

|Function | Improved function| Description|
| :------ | :--------------: | :--------- |
|stpcpy   | stpncpy          | Copy a string to another string and returns a pointer to its end. 
|strcpy   | strncpy          | Copies a string to another one.| 
|strcat   | strncat          | Concatenates two strings passed as a parameter into another one.| 
|strcmp   | strncmp          | Compares two strings passed as parameters to the function. Returns 0 if the strings are equal, something else if not. |
|strdup   | strndup          | Duplicates a string. 
|strlen   | strnlen          | Returns the total length of a string. |

*Table 1: String functions in C with their improved versions [^2]*

As you may have noticed, the improved versions all allow us to specify the 
total amount of data that should be considered when performing the operations. 
This gives a sense of protection against buffer overflow attacks. However, there is still
a risk of attacks defined as adjacent memory buffer overflow attacks. They
principally exploit the very structure of strings in the C programming language
and in the following section I am going to explain briefly how
and why it works. A subsequent blog post will provide a practical
exploit of the vulnerability. 

## The principle behind adjacent memory buffer overflow attacks

In the C programming language and its derivatives, a string is an
array of characters that is ended by a null byte (`x00` or `0`). 
The null character here represents the end of the string and
is used as such by the compiler. It means that at execution
time, the program will consider a string to start by its location
in memory up to the first null byte found. The figure below
shows briefly how this works. 

![Overview of a string in the execution stack of a program](/images/2020/05/string-1.png)

This structure ensures that a string can
be read without accessing another memory address. However, the compiler provides no
way of checking that the last character is actually a null byte.
This poses a problem that cannot be fixed by specifying the length
to be considered for various string operations. This is the basic principle
that explains the feasibility of adjacent memory buffer overflow attacks. The way
it works is that you have two strings that occupy adjacent memory
locations (i.e the end of the first one coincides with the start
of the other one). 

Using one string function that specifies the length
to consider such as strncpy for a string of length n, this
will allow the user to input exactly n characters that could be
different than the null bytes. This means that the n-th character of
the string could be something different than the null byte. This already
constitutes a buffer overflow as the string has occupied its whole space
and is still not terminated (absence of the null byte). Consequently, for
every character that will be added to the second string, an attempt
to read the first string will lead to accessing both strings concatenated.
Let us look at an illustration of this. 

![Adjacent memory buffer overflow attack explained in images](/images/2020/05/image_attack.png)

As you can see in the image, there
is no null byte between both strings. As such any attempt to
read string 1 like 

```
printf(%s, string1)
``` 

will output **Hello world!! from Vinny**
which is not normal. This is where the vulnerability comes from and
it can be exploited to override return addresses as we will see
in part two of this article. 

## Why does it matter and how can this be exploited? 

In a general sense, a vulnerability like this
could lead to the execution of malicious code especially if measures such
as ASLR (Address Space Layout Randomization) and inexecutable shell are not implemented.
A non-exhaustive list of actions that can be performed by exploiting this
comprises privilege escalation, access to a remote shell, access to private data
on a server, or even daisy chaining. Unfortunately, most of the IoT
devices on the market are vulnerable to this attack and if by
any chance a program makes use of those functions, this represents a
serious threat to information systems. Part two of this article will guide
through an attack scenario, identifying the vulnerability, writing the payload in Assembly
code, and then proceed with smashing the stack to execute it. 

## References 

[^1]: Livshits, V. Benjamin, and Monica S. Lam. “Finding Security Vulnerabilities     in Java Applications with Static Analysis.” USENIX Security Symposium. 
     Vol. 14. 2005. 

[^2]: Man7.org. 2020. String.H.0P – Linux Manual Page. [online]
     Available at: <http://man7.org/linux/man-pages/man0/string.h.0p.html> 
     [Accessed 18 May 2020].
