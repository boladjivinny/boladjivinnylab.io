---
title: Write-up of a reverse engineering challenge in PicoCTF2021
date: 2021-04-03T16:00:00+02:00
layout: post
image: /images/cyber-security-3400657_1280.jpg
tags:
  - PicoCTF2021
  - Reverse Engineering
---

This last month, I had the opportunity to participate to yet another CTF in my
cybersecurity specialist journey. And this was none less than the globally 
acclaimed PicoCTF organized yearly by Carnegie Mellon University.

While I did not really get time to spend on the competition as I wish I 
would, I still managed to walk through some interesting challenges. In this post,
I am going to present the challenge and how I went about solving it. It is 
probably not the hardest challenge but it sounded too cool for me and I also
wanted a primer in public write-ups so ....

Let's get back to business. The aforementioned challenge was named
**Transformation** and consisted in deciphering an encrypted message given the
code that generated it. Below the description of the challenge:

```
I wonder what this really is... enc ''.join([chr((ord(flag[i]) << 8) + ord(flag[i + 1])) for i in range(0, len(flag), 2)])
```

And the encrypted message below

```
灩捯䍔䙻ㄶ形楴獟楮獴㌴摟潦弸彥ㄴㅡて㝽
```

At first, I wanted to find an online decoder for the function but I could
not figure out what encryption scheme it was - if any. So I decided to reverse
the function.

As noticed in the code, the encryption works on a pair of letters at once:

1. It first shifts the integer equivalent of letter `i` by 8 bits to the left.
2. Adds the equivalent of the second letter to the value previously gotten.

From there, it means that we can get the first letter by shifting back by 8
bits to the right - normally we would lose some information but it seemed to
work. Then we retrieve the second letter by substracting that first value from
the integer equivalent of the letter being processed. We can then combine the
results in order to recover the message back.

Below is the code that I used for the matter:


```python
with open('/tmp/enc') as f:
    flag = f.readline()
    odd = ''.join([
        chr((ord(flag[i]) >> 8))
        for i in range(len(flag))
    ])

    even = ''.join([
        chr(ord(flag[i]) - ((ord(flag[i]) >> 8) << 8)) 
        for i in range(len(flag))
        ])
    the_flag = ""
    for i in range(len(odd)):
        the_flag += odd[i] + even[i]
    print(the_flag)
>>> picoCTF{16_bits_inst34d_of_8_e141a0f7}
```

And by doing so, I was able to get the flag and move to other challenges.
Among other things I learned during the challenge was the fact that one can
decipher an RSA encrypted message if a certain oracle is available to 
decipher some other messages. Very interesting topic that you can read more
about [here][oracle-attack].



[oracle-attack]: https://bitsdeep.com/posts/attacking-rsa-for-fun-and-ctf-points-part-1/
