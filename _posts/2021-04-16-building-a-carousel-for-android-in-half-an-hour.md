---
title: How I built a carousel for Android in less than half an hour
date: 2021-04-16T21:35:00+02:00
layout: post
image: /images/android-slider.png
tags:
  - Android
  - Java
---
My journey in Android development dates back to 2018 when I was interning in a HR consulting company as full stack developer.
Before that, I have mostly been running away from mobile development for the simple reason that my laptop was messing around
while trying to execute Android Studio (or Eclipse as a matter of fact). But that was really the least of my problems. In fact,
while I was interning, I literally had no one to supervise me and guide me through the architecture so I had to deal with uncle
Google most of the time to find my way around. Fortunately, I was not the only one having a hard time with Android. Talking of this
reminds me of an elder by then who used to pronounce **layout** as "_layut_" (written using the [international phonetic alphabet](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet)) 😆.

Long story short, I am not here to talk about my programming adventures even though looking back, it just reminds me of how 
versatile and motivated I can be. So at that job, my first Android related task had to do with taking an existing application
and finish it. Among the things I implemented was a carousel of images of various products. Having [used Bootstrap](http://getbootstrap.com/) a few times,
I got shocked at how difficult it was to create such a simple thing in Android. Or so was I thinking...
I ended up using an external library there, and for a number of other projects after that. Today, once again I had such a use 
case and my first reaction was to look up a solution on the Internet. But this time around, I felt like those solutions were too big
and complex for my simplistic needs. I only needed something to skim through a number of images so I was like let's try something 🧐.

# The thought process
Basically for a carousel, I need a way to first display images with the only major condition being that it is as fast as possible.
The second requirement is the ability to select what image to see. How can we handle that in Android?

Well, for the image displaying part, while there is a number of tools out there, I had a good experience with [Picasso](https://github.com/square/picasso)
so I decided to stick with that. Why I like it is because it provides some features like caching, callbacks (a bit ugly though) and
image editing prior to displaying.

Regarding the way in which we can loop through the images, I used a [SeekBar](https://developer.android.com/reference/android/widget/SeekBar) as a way to retrieve the index of the image that the user wants to access.

With that build-up, we obtain roughly the application in the video below (credit photo https://xkcd.com)

<iframe width="1280" height="765" src="https://www.youtube.com/embed/hayDRfVBKBY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Building the application
1. First we add the dependencies the the app level gradle file as follows:

```
dependencies {
    ...
    implementation 'com.squareup.picasso:picasso:2.71828'
    ...
}
```

This ensures that we can use the Picasso classes from within the application.

2. Next we create an activity along with its layout. See below the layout for the activity's screen.

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity"
    android:padding="@dimen/default_padding">

    <TextView
        android:id="@+id/title_label"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="XKCD images"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        style="@style/TextAppearance.AppCompat.Title" />

    <ImageView
        android:id="@+id/image_zone"
        android:layout_width="match_parent"
        android:layout_height="400dp"
        app:layout_constraintTop_toBottomOf="@id/title_label"
        app:layout_constraintLeft_toLeftOf="parent"
        />

    <SeekBar
        android:id="@+id/slider"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:min="1"
        app:layout_constraintTop_toBottomOf="@id/image_zone"
        app:layout_constraintLeft_toLeftOf="parent" />

    <ProgressBar
        android:id="@+id/progress"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

3. Another important part of the setup is to add the internet permission to the application in the AndroidManifest.xml file:

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="coach.vinny.slidingmaster">
    <uses-permission android:name="android.permission.INTERNET" />
    ...
</manifest>
```

From there, we can start constructing the application.
We first need a list of images statically set here:

```java
private final String [] imagesLinks = {
            "https://imgs.xkcd.com/comics/post_vaccine_social_scheduling.png",
            "https://imgs.xkcd.com/xkcloud/splash.png",
            "https://imgs.xkcd.com/comics/puzzle.png",
            "https://imgs.xkcd.com/comics/data_pipeline.png",
            "https://imgs.xkcd.com/comics/rule_34.png",
            "https://imgs.xkcd.com/comics/orphaned_projects.png",
            "https://imgs.xkcd.com/comics/quantum_teleportation.png",
            "https://imgs.xkcd.com/comics/artifacts.png",
            "https://imgs.xkcd.com/comics/tinder.png",
            "https://imgs.xkcd.com/comics/worst_hurricane.png"
    };
// Other variables
private ImageView display;
private ProgressBar progressBar;
```

Then a way to load an image. For this we create a function that takes an index and loads the corresponding image. The function
``loadImage`` below does just that. It uses the Picasso library to load and render the image into the ImageView. It also displays
the progress bar while loading and for debugging purposes, logs errors to the console.

```java
    private void loadImage(int index) {
        /*
        This function loads the image at the specified index into the ImageView.
         */
        Picasso.get().load(imagesLinks[index]).into(display, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                Log.e("SLIDER", "Failed to load image", e);
            }
        });
    }

```

We can then go ahead now and use our code in the activity. For this, we override the ``onCreate`` method of the Activity class to 
initialize the views that we need and most importantly, add a listener on the ``SeekBar`` to trigger the update of the image
being displayed.

```java
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.image_zone);
        SeekBar slider = findViewById(R.id.slider);
        progressBar = findViewById(R.id.progress);

        // Load the first image
        loadImage(0);

        // set the maximum of the slider to the length of our array
        slider.setMax(imagesLinks.length);
        // add a change listener to the slider/seekbar
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // The progress variable represents which image we want to display. 1 indexing
                loadImage(progress - 1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
```

As you may have noticed, while loading the image from the listener, ``1`` is susbstracted from the value. This is because the
bar has a 1-indexing while the array's index starts from zero.

Upon doing that, build the application, run it and enjoy your carousel. You can access the source code [here](https://github.com/boladjivinny/minimal-android-carousel).

Shalom! 😉
