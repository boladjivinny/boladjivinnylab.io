---
title: It took away my French but gave more to the one in my flesh
date: 2021-06-02T00:00:01+02:00
layout: post
image: /images/2021/desk-writing-poem.jpg
---

It was on a sunny day...\
Ready to break the limits set by the sky\
Sitting on that couch, the only thing to be done was to try\
To go past the emotions and enjoy the moment that cost so much pray-ers\
A dream come true one would say.

On my way to live that "African" dream\
Well installed inside the giant's belly, what felt like a film\
Was set to provoke so many changes.\
As the saying goes, big things require big sacrifices.

And I was ready to sacrifice a lot, including my French\
Little did I know it would happen effortlessly.\
And before I even realized it, words started coming hardly\
But I have seen myself grow in so many other aspects\
Turning into someone I can just respect\
The fella who lost his French to the benefit of he who lives in his flesh.

Sacrificed on the altar of my adventures\
I mourn friendships and relationships\
Beliefs, dead ambitions, and conceptions.

But just like a phoenix rises from the ashes\
The son of the man came out stronger.\
Dreams making space for goals\
And stupidity shrinking one day after another.

To the dead friendships, great substitutes came in\
Cheering to growth and awesomeness\
I am grateful for my **F** and **L**\
Together, they form my own version of the **Eifel**\
And while I take the time to build my Paris\
I enjoy the process, learn from it and move on with no stress.

Today I felt the need to write but I want to be short\
For a while now I have been looking for the poet\
The one I lost some years ago\
Maybe he is back, maybe not\
But what is sure, I am there and I am to stay\
To myself, or the one in my flesh, I want to say\
Happy birthday!