---
title: "Blame game, lame lane"
date: 2023-01-07T13:19:01-05:00
layout: post
image: /images/2023/plant-g55d4c23a7_1280.jpg
---

According to history, two major theories exist on the matter of the world’s creation. 
While one supports the evolutionary scheme of things that made the world what it is 
today, the second view attributes the universe's and humanity’s existence to a 
demiurge. In the latter, a man and a woman are believed to have been living in 
a garden where all were provided. They were living a perfect life until they 
both ate the only fruit the creator had forbidden them to. The mistake followed 
a discussion during which the snake convinced the woman to eat the fruit. 
According to the story, we owe to this treachery our very conscience of 
“what is good and what is bad”. Driven by their awareness, the two humans 
decided to hide from the creator. What happens next is the exciting part. 
Upon questioning the man about his motives for disobeying, his answer was 
unequivocal: the woman was the one to blame. In return, the woman blamed 
the mistake on the snake, who got the worst of the curses from the deity. 
If you are familiar with this story, you probably have guessed that the 
two individuals are Adam and Eve (Genesis 2 & 3). I read the same narrative 
in the five most popular English translations of the Bible in the United States 
of America, and they all agree that Adam and Even, the said first humans ever 
to exist, have all played the game of blame.

My choice to start with this story is to make the point that blaming is endemic 
to humanity in a way that even children whom Jesus calls us to be like (Matthew 18:1-3), 
also fall victim to it. In that sense, I believe that not owning to one’s mistakes 
is the real ‘original sin’, and not sex as in popular conception. In the next few 
points, I describe why I think blame is a self degrading act and how one can get 
out of its trap.

## _“My people are destroyed for lack of knowledge.”_ - Hosea 4:6
In the Adam and Eve’s story shared earlier, two main factors stand out as leading to
 the act of blaming: guilt, and ignorance. The guilt is expressed in the hiding of 
 the two individuals from the face of God. There might be a point to be made for the
  fact that their reaction was rather motivated by shame but I do not think that shame
   exists without guilt. The perfect illustration of that is seen in kids who can put
themselves in situations that adults would consider so embarrassing that are just very 
normal to them. On the other side, children like the one in the parable of the 
prodigious son, who are aware of their wrongdoing display that sense of shame that is 
precursor to finding someone or something to blame. 

The second thing is ignorance which in the case of Adam is to be found in that he was 
not aware of his own willpower. Eve on the other side clearly lacked the insight that 
she needed to contradict the snake. By leaving them in the blank or in the state of 
dogma, the creature made no service to these two individuals who are till date, 
covertly considered responsible for the fate of humanity by many. Besides the 
fictional aspect, the consequences of ignorance induced blame can get really ugly. For 
instance, the earlier centuries of our era has seen diseases kill many of our 
ancestors because they could not understand what was happening and attributed those 
diseases to the anger of the gods. But then, they did the best they could with what 
they knew at that point. This leads me to the elements that, in my humble opinion 
could help us combat the urge to blame others.

## _“To know thyself is the beginning of wisdom.”_ - Socrates
One of the ugliest blame in the world is that of rapists who attribute their act to 
the physic or dressing of the victim, which in many societies is still valued as a 
valid argument. Much like it was the case with Adam or in the story of Samson with 
Delilah (Judge 16), this conception withdraws the main actor from the responsibility 
of the decision that they made. It simplifies the problem to a trigger-consequence 
sort of relationship and completely disregard the capacity of the rapist to choose 
their course of action. Consequently, they fail to understand themselves and value 
their capacity to control their thoughts. Thus, their willpower muscle gets extremely 
weak over time, while the blame muscle tends to grow bigger. In that sense, it is 
necessary to apply meta-cognition (thinking about our thoughts), seek clarity and 
knowledge, in order to make better decisions. From that point on, one should learn to 
take responsibility for all the acts they have committed, and recognize them as such. 
It is so easy to say “I am not going outside because it is too cold” than “I choose 
not to expose myself to the cold by not going outside”. The two sentences express the 
same message but one empowers you while the other lets you hide behind the weather 
that is not against you. From that point on, you could even find solutions to the 
problem you were initially about to withdraw your responsibility from such as putting 
on layers to stay warm. Whatever the solution, the key is that you get yourself to 
think of solutions.
