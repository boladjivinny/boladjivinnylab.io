---
title: 5 strategies I used in my journey to multilingualism
date: 2019-12-31T13:31:40+00:00
layout: post
image: /images/2019/12/welcome-976277_1920-1.jpg
tags:
  - Discovery
  - Lifestyle
  - Travel
---
Some months ago, I was to sit for my TOEFL test in order to get admitted to a master's program I wanted to join very very deeply. While I did very well, I was not quite confident of my English skills until I got to Rwanda and started my studies. This is when I realized that my English has reached a good level and I felt good with myself. Not just because I could study easily in my new environment but most importantly because it gives me a broad range of new opportunities I could apply to. This is not just for me, but for all of us because at some point the language can constitute a barrier to the full achievement of our potential. I know my journey might not be the most impressive, but it might help you find your own way to attain multilingualism, to _become a polyglot_.

![Multilingualism meme image](https://crystalcleartranslation.com/wp-content/uploads/2019/09/interacció.jpg)

## How my journey to multilingualism started

Having grown up in Benin where a great number of languages are spoken, I quickly learnt to speak two of them : **Fongbe and Goun**. Thanks to my exceptional mom, I learnt how to read and write in the former, which happens to have a very difficult syntax to deal with. This clearly illustrates how easy it is to learn a language as a kid when the environment allows it. But let's talk about how, almost on my own, I was able to learn and get fluent at English.

The journey started way back in my last year of high school where my English instructor required us to only speak English during the class. Having observed that rule, my proficiency gradually increased and I got to a somehow comfortable level. One thing I realized in this journey is that school cannot really teach someone a language to perfect mastery. I know this might seem silly to you but all those translations, grammar, pronunciation, etc. classes are not really useful in learning how to **speak **a language but those who experienced it can relate. Anyway, I stand to be corrected.

Through my own personal journey though, here are the few techniques that turn out to have helped me the most.

![](/images/2019/12/eprs-aag-625198-remaining-united-in-diversity-thanks-to-multilingualism-final.jpg)

### 1. Read a lot of books in the target language

This is a very important aspect of learning a new language. Whether you're a fan of anime, novels, magazines or other kinds of books, make a point of reading as much as you can in the language you intend to learn. For me, I like reading self-help and entrepreneurial books and this is what I was doing most of the time. I also made a point of not reading a book in French anymore  _I don't even remember the last time I totally read a French book_. I was also fortunate enough to have an Oxford dictionary in which I was checking the meaning of words that I didn't know. I cannot count the number of words and useful sentences I learnt this way.

![](/images/2019/12/the_economist_guide_to_management_ideas_and_gurus_1556106829_ecb67f93.jpg)

### 2. Practice making a passive listening

_Just try a minute to reflect on how you learnt your mother tongue._ 

I'm sure most of us don't know how it happens yet it's the very first language you ever spoke. How can you explain that without any language background you nailed a language that easily. My own opinion is that it had something to do with the universal language mentioned by Santiago in the Alchemist. So, to learn a new language, sometimes I think that you just need to let the words flow in your mind without trying to understand them. The risk in trying to translate the words in your language is that you'll lose sight of what is being said and at some point, you get lost.

Well I actually don't know how it works but what I noticed is that it works very well. So my own way of doing it is to go jogging and have my earphones playing a content in English. In those times, I'm barely able to think about anything and then my brain can quickly grasp what is being said  _don't under estimate your brain please_. Some people even suggest sleeping while listening to the content but I personally cannot tell how well this works.

### 3. Speak as much as you can

Having spent most of my school time in Benin, I know the struggle to find someone with whom to speak English easily. Of course, they were English clubs but none of them fitted in my agenda so I had to improvise. Thankfully, I got this Kindle book ([English Fluency for Advanced English Speaker](https://www.amazon.com/gp/slredirect/picassoRedirect.html/ref=pa_sp_atf_stripbooks_sr_pg1_1?ie=UTF8&adId=A04814741Y25WX8PIZDYF&url=%2FEnglish-Fluency-Advanced-Speaker-Potential%2Fdp%2F1514632284%2Fref%3Dsr_1_1_sspa%3Fkeywords%3DEnglish%2BFluency%2Bfor%2BAdvanced%2BEnglish%2BSpeaker%26qid%3D1577797374%26s%3Dbooks%26sr%3D1-1-spons%26psc%3D1&qualifier=1577797374&id=5509256358208402&widgetName=sp_atf)) in which the author suggested a very unexpected way to practice a language. As simple as it is, it consists in watching or listening to a content that you like (mine is listening to the radio and watching TED talks), and try to repeat the sentences that you were able to hear. The reproduction should try to mimic as well as possible, the accent, tone and voice of the speaker. By doing that, not only you are learning the language but you are also, effortlessly, getting the accent that you want to have. **Incredible and it works.**

![](/images/2019/12/tenor.gif)

### 4  Update the language in your head

Every one must agree that speaking is not just the act of talking but it all starts somewhere : in our mind. And the usual way for people learning a new language is to speak the sentence in the language they are most comfortable with and then to translate it. It does work sometimes but for sentences that don't follow the same pattern in both languages, you just get stuck or spend more time trying to find the right words. The effect is that you lose confidence and get scared of speaking your words.

On the other side, when you think in the target language, you can easily speak it out without even noticing the effort. And to make it a personal fun, you can try to translate it back to your main language so all your thinking is done in this language you want to learn.

![](/images/2019/12/frenc-english-meme.png)

### 5  Never be scared of expressing yourself

As a native English speaker was saying last time, no one has a perfect mastery of a language. And most importantly, Practice makes perfect as the saying goes. So just let off your fear of being laughed at and talk your thoughts out with people whenever you can. By doing so, you'll get feedback to improve your level and continue your learning journey.

![](/images/2019/12/a-journey-of-a-thousand-miles-begins-with-a-single-step.jpg)

* * *

So that is all for my advice regarding being a polyglot. I hope that it helps you in your own journey to multilingaulism. Personally I intend to learn Kinyarwanda for the year 2020 [to feel more Rwandan ](https://vinny.coach/quid-of-boladji-vinny/)and get another language on my list of language spoken. How about you, which language are you willing to learn in the coming year or what strategies have worked in your personal journeys ?

_Happy new year 2020._ 

_Tubifurije umwaka mwiza wa 2020._
